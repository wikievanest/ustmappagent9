<?php
/**
 * @FileInfo: /Users/wikie/Development/Sites/ustmapp7/apps/modules/searchpatient/models/Searchpatient_qry.php
 * @Author: wikie
 * @Email: wikieonline@gmail.com
 * @Date: 2016-05-25 22:07:47
 * @Last Modified by:   wikie
 * @Last Modified time: 2017-08-19 05:01:02
 */

class Searchpatient_qry extends CI_Model{
    //put your code here
    protected $emr;
    public function __construct() {
        parent::__construct();
    }

    public function get_all_doctor() {
        $sql = "SELECT
                  a.fname AS fname,
                  a.lname AS lname
                FROM (emrapp15.users a
                JOIN emrapp15.form_encounter b
                  ON ((b.provider_id = a.id)))
                GROUP BY a.fname,
                         a.lname
                HAVING (a.fname <> 'Administrator')";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->result();

            return $data;
            $this->db->close();
        }
        return false;
    }

    public function get_states() {
      $sql = "SELECT zone_code, zone_name
              FROM emrapp15.geo_zone_reference";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->result();

            return $data;
            $this->db->close();
        }
        return false;
    }


    public function get_all_patient($emr,$first_name,$last_name) {
        if($first_name <> '') {
            $fname = "AND fname LIKE '%$first_name%' ";
        } 
        if($last_name <> '') {
            $lname = "AND lname LIKE '%$last_name%'";
        }
        $where = "WHERE emr LIKE '%$emr%' $fname $lname";
        $sql = "SELECT
                  pid,
                  fname,
                  mname,
                  lname,
                  dob,
                  state,
                  postal_code,
                  date,
                  city,
                  emr
                FROM all_patients_detail
                $where";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->result();

            return $data;
            $this->db->close();
        }
        return false;
    }
    public function get_detail_encounter($provider,$emr) {
        $db = 'emr'.$emr;
        $this->emr = $this->load->database($db,TRUE);
        $sql = "SELECT b.mdfname,c.fname,c.lname,c.DOB,a.reason,a.sensitivity,a.date,a.jml,a.facility 
            from (select  pid,provider_id,COUNT(id) jml,reason,sensitivity,DATE_FORMAT(date,'%Y-%m-%d') date,facility from form_encounter
            group by pid,provider_id
            HAVING provider_id <> 1) a
            INNER JOIN (select id,fname mdfname,mname mdmname,lname mdlname from users) b
            ON b.id=a.provider_id
            INNER JOIN (select pid,fname,lname,DOB from patient_data) c
            ON c.pid=a.pid
            where b.mdfname='$provider'
            order by a.date DESC";
        $query = $this->emr->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->result();

            return $data;
            $this->db->close();
        }
        return false;
    }

    public function get_emr_count() {
        $sql = "SELECT * from emr_active";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->row();

            return $data->jml;
            $this->db->close();
        }
        return false;
    }
}
