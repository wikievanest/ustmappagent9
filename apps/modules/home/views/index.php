<?php
 function getValue($a) {
  if(empty($a)) {
    return "0";
  } else {
    return $a;
  }
 }
?>
<script type="text/javascript">
$(function() {

    patientoption = {
      tooltip : {
        trigger: 'axis'
      },
      legend: {
        data:['<?php echo $this->emrs->emr;?>']
      },
      calculable : true,
      xAxis : [
        {
          type : 'category',
          data : ['January','February','March','April','May','June','July','August','September','October','November','December']
        }
      ],
      yAxis : [
        {
          type : 'value'
        }
      ],
      series : [
      <?php
        foreach ($patientcount as $pcn) {
      ?>
        {
          name:'<?php echo $pcn->emr;?>',
          type:'line',
          data:[<?php echo getValue($pcn->January).','.getValue($pcn->February).','.getValue($pcn->March).','.getValue($pcn->April).','.getValue($pcn->May).','.getValue($pcn->June).','.getValue($pcn->July).','.getValue($pcn->August).','.getValue($pcn->September).','.getValue($pcn->October).','.getValue($pcn->November).','.getValue($pcn->December);?>],
          markPoint : {
            data : [
              {type : 'max', name: 'Max'},
              {type : 'min', name: 'Min'}
            ]
          }
        },
        <?php
            }
        ?>
      ]
    }

    encounteroption = {
        tooltip : {
            trigger: 'axis'
        },
        legend: {
            data:['<?php echo $this->emrs->emr;?>']
        },
        toolbox: {
            show : false,
            feature : {
                mark : {show: true},
                dataView : {show: true, readOnly: false},
                magicType : {show: true, type: ['line', 'bar']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : true,
        xAxis : [
            {
                type : 'category',
                data : ['January','February','March','April','May','June','July','August','September','October','November','December']
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
        <?php
        foreach ($encountercount as $ecn) {
        ?>
            {
              name:'<?php echo $ecn->emr;?>',
              type:'bar',
              data:[<?php echo getValue($ecn->January).','.getValue($ecn->February).','.getValue($ecn->March).','.getValue($ecn->April).','.getValue($ecn->May).','.getValue($ecn->June).','.getValue($ecn->July).','.getValue($ecn->August).','.getValue($ecn->September).','.getValue($ecn->October).','.getValue($ecn->November).','.getValue($ecn->December);?>],
              markPoint : {
                data : [
                  {type : 'max', name: 'Max'},
                  {type : 'min', name: 'Min'}
                ]
              }
            },
        <?php
            }
        ?>
        ]
    };

    echarts.init(document.getElementById('ecnountercount')).setOption(encounteroption);
    echarts.init(document.getElementById('patientcount')).setOption(patientoption);
});

</script>
<section class="app-content">
    <div class="row">
      <div class="col-md-3 col-sm-6">
        <div class="widget stats-widget">
          <div class="widget-body clearfix">
            <div class="pull-left">
              <h3 class="widget-title text-primary"><span class="counter" data-plugin="counterUp"><?php echo $patient_count->jml;?></span></h3>
              <small class="text-color">Patients</small>
            </div>
            <span class="pull-right big-icon watermark"><i class="fa fa-paperclip"></i></span>
          </div>
          <footer class="widget-footer bg-primary">
            <small>Added today from all EMR</small>
            <span class="small-chart pull-right" data-plugin="sparkline" data-options="[4,3,5,2,1], { type: 'bar', barColor: '#ffffff', barWidth: 5, barSpacing: 2 }"></span>
          </footer>
        </div><!-- .widget -->
      </div>

      <div class="col-md-3 col-sm-6">
        <div class="widget stats-widget">
          <div class="widget-body clearfix">
            <div class="pull-left">
              <h3 class="widget-title text-danger"><span class="counter" data-plugin="counterUp"><?php echo $appointment_count->jml;?></span></h3>
              <small class="text-color">Appointments</small>
            </div>
            <span class="pull-right big-icon watermark"><i class="fa fa-ban"></i></span>
          </div>
          <footer class="widget-footer bg-danger">
            <small>Added today from all EMR</small>
            <span class="small-chart pull-right" data-plugin="sparkline" data-options="[1,2,3,5,4], { type: 'bar', barColor: '#ffffff', barWidth: 5, barSpacing: 2 }"></span>
          </footer>
        </div><!-- .widget -->
      </div>

      <div class="col-md-3 col-sm-6">
        <div class="widget stats-widget">
          <div class="widget-body clearfix">
            <div class="pull-left">
              <h3 class="widget-title text-success"><span class="counter" data-plugin="counterUp"><?php echo intval($encounter_count->jml);?></span></h3>
              <small class="text-color">Encounters</small>
            </div>
            <span class="pull-right big-icon watermark"><i class="fa fa-unlock-alt"></i></span>
          </div>
          <footer class="widget-footer bg-success">
            <small>Added today from all EMR</small>
            <span class="small-chart pull-right" data-plugin="sparkline" data-options="[2,4,3,4,3], { type: 'bar', barColor: '#ffffff', barWidth: 5, barSpacing: 2 }"></span>
          </footer>
        </div><!-- .widget -->
      </div>

      <div class="col-md-3 col-sm-6">
        <div class="widget stats-widget">
          <div class="widget-body clearfix">
            <div class="pull-left">
              <h3 class="widget-title text-warning"><span class="counter" data-plugin="counterUp">1</span></h3>
              <small class="text-color">Active EMR</small>
            </div>
            <span class="pull-right big-icon watermark"><i class="fa fa-file-text-o"></i></span>
          </div>
          <footer class="widget-footer bg-warning">
            <small>Based on EMR used</small>
            <span class="small-chart pull-right" data-plugin="sparkline" data-options="[5,4,3,5,2],{ type: 'bar', barColor: '#ffffff', barWidth: 5, barSpacing: 2 }"></span>
          </footer>
        </div><!-- .widget -->
      </div>
    </div><!-- .row -->

    <div class="row">
      <div class="col-md-12">
        <div class="widget">
          <header class="widget-header">
            <h4 class="widget-title">Real time patient added in <?php echo date('Y');?>  </h4>
          </header><!-- .widget-header -->
          <hr class="widget-separator">
          <div class="widget-body">
            <div data-plugin="chart" id="patientcount" style="height: 300px;">
            </div>
            <div class="row">
               <!-- Patient Count on the year -->
              <div class="col-lg-12" >
              <?php
                foreach ($patientcount as $pcm) {
                  $january[] = intval($pcm->January);
                  $february[] = intval($pcm->February);
                  $march[] = intval($pcm->March);
                  $april[] = intval($pcm->April);
                  $may[] = intval($pcm->May);
                  $june[] = intval($pcm->June);
                  $july[] = intval($pcm->July);
                  $august[] = intval($pcm->August);
                  $september[] = intval($pcm->September);
                  $october[] = intval($pcm->October);
                  $november[] = intval($pcm->November);
                  $december[] = intval($pcm->December);
                }
              ?>
                <div class="col-sm-1" style="border: 2px dotted red;padding: 5px;text-align: center">
                  Jan: <br><?php echo array_sum($january);?>
                </div>
                <div class="col-sm-1" style="border: 2px dotted red;padding: 5px;text-align: center">
                  Feb: <br><?php echo array_sum($february);?>
                </div>
                <div class="col-sm-1" style="border: 2px dotted red;padding: 5px;text-align: center">
                  Mar: <br><?php echo array_sum($march);?>
                </div>
                <div class="col-sm-1" style="border: 2px dotted red;padding: 5px;text-align: center">
                  Apr: <br><?php echo array_sum($april);?>
                </div>
                <div class="col-sm-1" style="border: 2px dotted red;padding: 5px;text-align: center">
                  May: <br><?php echo array_sum($may);?>
                </div>
                <div class="col-sm-1" style="border: 2px dotted red;padding: 5px;text-align: center">
                  Jun: <br><?php echo array_sum($june);?>
                </div>
                <div class="col-sm-1" style="border: 2px dotted red;padding: 5px;text-align: center">
                  Jul: <br><?php echo array_sum($july);?>
                </div>
                <div class="col-sm-1" style="border: 2px dotted red;padding: 5px;text-align: center">
                  Aug: <br><?php echo array_sum($august);?>
                </div>
                <div class="col-sm-1" style="border: 2px dotted red;padding: 5px;text-align: center">
                  Sep: <br><?php echo array_sum($september);?>
                </div>
                <div class="col-sm-1" style="border: 2px dotted red;padding: 5px;text-align: center">
                  Oct: <br><?php echo array_sum($october);?>
                </div>
                <div class="col-sm-1" style="border: 2px dotted red;padding: 5px;text-align: center">
                  Nov: <br><?php echo array_sum($november);?>
                </div>
                <div class="col-sm-1" style="border: 2px dotted red;padding: 5px;text-align: center">
                  Dec: <br><?php echo array_sum($december);?>
                </div>
              </div>
            </div>
          </div><!-- .widget-body -->
        </div><!-- .widget -->
      </div><!-- END column -->
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="widget">
          <header class="widget-header">
            <h4 class="widget-title">Real time encounter added in <?php echo date('Y');?>  </h4>
          </header><!-- .widget-header -->
          <hr class="widget-separator">
          <div class="widget-body">
            <div data-plugin="chart" id="ecnountercount" style="height: 300px;">
            </div>
            <div class="row">
             <!-- Patient Count on the year -->
              <div class="col-lg-12">
              <?php
                foreach ($encountercount as $ecm) {
                  $ejanuary[] = intval($ecm->January);
                  $efebruary[] = intval($ecm->February);
                  $emarch[] = intval($ecm->March);
                  $eapril[] = intval($ecm->April);
                  $emay[] = intval($ecm->May);
                  $ejune[] = intval($ecm->June);
                  $ejuly[] = intval($ecm->July);
                  $eaugust[] = intval($ecm->August);
                  $eseptember[] = intval($ecm->September);
                  $eoctober[] = intval($ecm->October);
                  $enovember[] = intval($ecm->November);
                  $edecember[] = intval($ecm->December);
                }
              ?>
                <div class="col-sm-1" style="border: 2px dotted red;padding: 5px;text-align: center">
                  Jan: <br><?php echo array_sum($ejanuary);?>
                </div>
                <div class="col-sm-1" style="border: 2px dotted red;padding: 5px;text-align: center">
                  Feb: <br><?php echo array_sum($efebruary);?>
                </div>
                <div class="col-sm-1" style="border: 2px dotted red;padding: 5px;text-align: center">
                  Mar: <br><?php echo array_sum($emarch);?>
                </div>
                <div class="col-sm-1" style="border: 2px dotted red;padding: 5px;text-align: center">
                  Apr: <br><?php echo array_sum($eapril);?>
                </div>
                <div class="col-sm-1" style="border: 2px dotted red;padding: 5px;text-align: center">
                  May: <br><?php echo array_sum($emay);?>
                </div>
                <div class="col-sm-1" style="border: 2px dotted red;padding: 5px;text-align: center">
                  Jun: <br><?php echo array_sum($ejune);?>
                </div>
                <div class="col-sm-1" style="border: 2px dotted red;padding: 5px;text-align: center">
                  Jul: <br><?php echo array_sum($ejuly);?>
                </div>
                <div class="col-sm-1" style="border: 2px dotted red;padding: 5px;text-align: center">
                  Aug: <br><?php echo array_sum($eaugust);?>
                </div>
                <div class="col-sm-1" style="border: 2px dotted red;padding: 5px;text-align: center">
                  Sep: <br><?php echo array_sum($eseptember);?>
                </div>
                <div class="col-sm-1" style="border: 2px dotted red;padding: 5px;text-align: center">
                  Oct: <br><?php echo array_sum($eoctober);?>
                </div>
                <div class="col-sm-1" style="border: 2px dotted red;padding: 5px;text-align: center">
                  Nov: <br><?php echo array_sum($enovember);?>
                </div>
                <div class="col-sm-1" style="border: 2px dotted red;padding: 5px;text-align: center">
                  Dec: <br><?php echo array_sum($edecember);?>
                </div>
              </div>
            </div>
            </div>
          </div><!-- .widget-body -->
        </div><!-- .widget -->
      </div><!-- END column -->
      <?php 
      if($patient_count->jml > 0) {
    ?>
    <div class="row">
      <div class="col-md-12">
        <div class="widget">
          <header class="widget-header">
            <h4 class="widget-title">Today Patients</h4>
          </header>
          <hr class="widget-separator"/>
          <div class="widget-body">
            <div class="table-responsive">              
              <table class="table">
                <thead>
                  <tr>
                    <th style="width: 4%;text-align:center">ID</th>
                    <th style="width: 20%">Full Name</th>
                    <th style="width: 10%;text-align:center">Date of birth</th>
                    <th style="width: 15%;text-align:left">City</th>
                    <th style="width: 10%;text-align:center">State</th>
                    <th style="width: 6%;text-align:center">Zip</th>
                    <th style="width: 10%;text-align:center">EMR</th>
                    <th style="width: 15%;text-align:center">Input Date</th>
                    <th style="width: 8%;text-align:center">Script Status</th>
                    <th style="width: 8%;text-align:center">CCR Status</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                  foreach ($patient as $pt) {
                ?>
                  <tr>
                    <td style="width: 4%;text-align:center"><?php echo $pt->pid;?></td>
                    <td style="width: 20%"><?php echo $pt->fname.', '.$pt->mname.' '.$pt->lname;?></td>
                    <td style="width: 8%;text-align:center"><?php echo $pt->dob;?></td>
                    <td style="width: 15%;text-align:left"><?php echo $pt->city;?></td>
                    <td style="width: 10%;text-align:center"><?php echo $pt->state;?></td>
                    <td style="width: 6%;text-align:center"><?php echo $pt->postal_code;?></td>
                    <td style="width: 10%;text-align:center"><strong><?php echo $pt->emr;?></strong></td>
                    <td style="width: 15%;text-align:center"><?php echo $pt->date;?></td>
                    <td style="width: 8%;text-align:center"><strong><?php echo $pt->script_status;?></strong></td>
                    <td style="width: 8%;text-align:center"><strong><?php echo $pt->ccr_status;?></strong></td>
                  </tr>
                  <?php
                    }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div><!-- .widget -->
      </div><!-- END column -->
    </div><!-- .row -->
    <?php 
      }
    ?>
    <?php 
      if($encounter_count->jml > 0) {
    ?>
    <div class="row">
      <div class="col-md-12">
        <div class="widget">
          <header class="widget-header">
            <h4 class="widget-title">Today Encounters</h4>
          </header>
          <hr class="widget-separator"/>
          <div class="widget-body">
            <div class="table-responsive">              
              <table class="table">
                <thead>
                  <tr>
                    <th style="width: 6%;text-align:center">ID</th>
                    <th style="width: 15%">Full Name</th>
                    <th style="width: 10%;text-align:center">DOB</th>
                    <th style="width: 15%;text-align:center">Facility</th>
                    <th style="width: 10%;text-align:center">Sensitivity</th>
                    <th style="width: 14%;text-align:center">MD</th>
                    <th style="width: 10%;text-align:center">EMR</th>
                    <th style="width: 15%;text-align:center">Date Input</th>
                    <th style="width: 5%;text-align:center">Script Status</th>
                    <th style="width: 5%;text-align:center">CCR Status</th>
                    
                  </tr>
                </thead>
                <tbody>
                <?php
                  foreach ($encounter as $ec) {
                ?>
                  <tr>
                    <td style="width: 6%;text-align:center"><?php echo $ec->encounter;?></td>
                    <td style="width: 15%"><?php echo $ec->fname.', '.$ec->mname.' '.$ec->lname;?></td>
                    <td style="width: 10%;text-align:center"><?php echo $ec->DOB;?></td>
                    <td style="width: 15%;text-align:center"><?php echo $ec->facility;?></td>
                    <td style="width: 10%;text-align:center"><?php echo $ec->sensitivity;?></td>
                    <td style="width: 14%;text-align:center"><?php echo $ec->mdfname.', '.$ec->mdmname.' '.$ec->mdlname;?></strong></td>
                    <td style="width: 10%;text-align:center"><strong><?php echo $ec->emr;?></strong></td>
                    <td style="width: 15%;text-align:center"><?php echo $ec->date;?></td>
                    <td style="width: 5%;text-align:center"><?php echo $ec->script_status;?></td>
                    <td style="width: 5%;text-align:center"><?php echo $ec->ccr_status;?></td>
                  </tr>
                  <?php
                    }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div><!-- .widget -->
      </div><!-- END column -->
    </div><!-- .row -->
    <?php 
      }
    ?>
      </div>
    </div><!-- END row -->
  </section><!-- #dash-content -->