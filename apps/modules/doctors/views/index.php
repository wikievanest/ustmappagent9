<script type="text/javascript">
  $(document).ready(function() {
    $('#responsive-datatable').DataTable({
      "pageLength": 25
    });

} );

  function assign(eid) {
      $("#eid").val(eid);
      $("#myModal").modal('show');
    }
</script>
<section class="app-content">
    <div class="row">
      <!-- DOM dataTable -->
      <div class="col-md-12">
        <div class="widget">
          <header class="widget-header">
            <h4 class="widget-title">USTM Providers</h4>
          </header><!-- .widget-header -->
          <hr class="widget-separator">
          <div class="widget-body">
            <table id="responsive-datatable" class="table" data-plugin="DataTable" cellspacing="0" width="100%">
              <thead>
                  <tr>
                    <th style="width: 4%;text-align:center">ID</th>
                    <th style="width: 20%">Full Name</th>
                    <th style="width: 10%;text-align:center">Status</th>
                    <!--<th style="width: 20%;text-align:center">Action</th> -->
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $i=1;
                  foreach ($doctors as $md) {
                  $status='<span class="label label-success">Active</span>';
                ?>
                  <tr>
                    <th style="width: 4%;text-align:center"><?php echo $i;?></th>
                    <th style="width: 20%"><?php echo $md->fullname;?></th>
                    <th style="width: 10%;text-align:center"><?php echo $status;?></th>
                  </tr>
                  <?php
                    $i++;
                    }
                  ?>
                </tbody>
                </tbody>
            </table>
          </div><!-- .widget-body -->
        </div><!-- .widget -->
      </div><!-- END column -->
    </div><!-- .row -->
  </section><!-- .app-content -->