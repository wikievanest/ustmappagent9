<?php
/**
 * @FileInfo: /Users/wikie/Development/Sites/ustmappbruce/apps/modules/patientbyencounter/models/Patientbyencounter_qry.php
 * @Author: wikie
 * @Email: wikieonline@gmail.com
 * @Date: 2016-05-25 22:07:47
 * @Last Modified by:   wikie
 * @Last Modified time: 2017-07-27 03:32:34
 */

class Patientbyencounter_qry extends CI_Model{
    //put your code here
    protected $emr;
    public function __construct() {
        parent::__construct();
    }

    public function get_all_doctor() {
        $sql = "SELECT * FROM all_doctors
                group by fname,lname
                order by fname";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->result();

            return $data;
            $this->db->close();
        }
        return false;
    }

    public function get_states() {
      $sql = "SELECT zone_code, zone_name
              FROM emrapp6.geo_zone_reference";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->result();

            return $data;
            $this->db->close();
        }
        return false;
    }


    public function get_all_patient($emr,$state, $input_date,$to_date) {
        if($emr <> '') {
          $emr = "EMR #$emr";
        } else {
          $emr='';
        }
        if($input_date <> '' AND $to_date <> '') {
            $filter = "AND date_encounter BETWEEN '$input_date' AND '$to_date'";
        } 
        if($state <> '') {
            $states = "AND state = '$state'";
        } 
        $where = "WHERE emr LIKE '%$emr%' $filter $states";
        $sql = "SELECT pid,
                  CONCAT(fname,', ',
                  mname,' ' ,
                  lname) fullname,
                  DOB,
                  state,
                  city,
                  postal_code,
                  date_encounter,
                  script_status,
                  ccr_status,
                  emr
                FROM all_patients_encounters
                $where
                order by date_encounter asc";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->result();

            return $data;
            $this->db->close();
        }
        return false;
    }
}
