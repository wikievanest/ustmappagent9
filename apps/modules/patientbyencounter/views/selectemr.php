<script type="text/javascript">
	$(function () {
        $('#input_date').datetimepicker({
            format: 'YYYY-MM-DD'
        });
        $('#to_date').datetimepicker({
            format: 'YYYY-MM-DD'
        });
        $('#emr').chosen();
        $('#state').chosen();
    });
	function redirect() {
		var emr = $("#emr").val();
		window.location.assign('<?=base_url('patientbyencounter');?>');
	}

</script>
<section class="app-content">
	<div class="row">
		<div class="col-md-12">
			<div class="widget">
				<header class="widget-header">
					<h4 class="widget-title">Total Patient by Encounters</h4>
				</header><!-- .widget-header -->
				<hr class="widget-separator">
				<div class="widget-body">
					<div class="m-b-xl">
							<?php 
								$submit = "patientbyencounter/all";
					            $attributes = array('role' => 'form'
					                , 'id' => 'form_add', 'name' => 'form_add','class' => 'form-inline', 'onSubmit' => 'document.getElementById(\'btn\').disabled=true;');
					            echo form_open($submit,$attributes); 
					        ?>
					        <div class="form-group">
								<select id="emr" class="form-control" name="emr">
									<?php
										foreach ($emrs as $emr) {
									?>
									<option value="<?php echo $emr->EMR_ID;?>"><?php echo $emr->EMR_NAME;?></option>
									<?php
										} 
									?>
								</select>
							</div>
							&nbsp;
							<div class="form-group">
								<select id="state" class="form-control" name="state">
									<option value="">All States</option>
									<?php
										foreach ($states as $state) {
									?>
									<option value="<?php echo $state->zone_code;?>"><?php echo $state->zone_code.' - '.$state->zone_name;?></option>
									<?php
										} 
									?>
								</select>
							</div>
							&nbsp;
							<div class="form-group">
								<div class="col-sm-2">
									<input style="min-width: 180px;" type="text" size="4" id="input_date" class="form-control" data-plugin="datetimepicker" name="input_date" placeholder="Date Encounter from" required>
								</div>
							</div>
							&nbsp;
							<div class="form-group">
								<input style="min-width: 180px;" type="text" size="1" id="to_date" class="form-control" name="to_date" placeholder="Encounter To Date" required>
							</div>
							&nbsp;
							<button type="submit" class="btn btn-primary" id="btn"><i class="menu-icon zmdi zmdi-search zmdi-hc-lg"></i> Search</button>
						<?php echo form_close(); ?>
					</div>
				</div><!-- .widget-body -->
			</div><!-- .widget -->
		</div><!-- END column -->
	</div><!-- .row -->
</section><!-- #dash-content -->