<?php
/**
 * @FileInfo: /Users/wikie/Development/Sites/ustmapp7/apps/libraries/Emrs.php
 * @Author: wikie
 * @Email: wikieonline@gmail.com
 * @Date: 2016-07-20 12:09:49
 * @Last Modified by:   wikie
 * @Last Modified time: 2017-08-19 04:58:33
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Emrs {

		public static $emr;
        public static $client_id;
        public static $emr_number;
        
		function __construct() {
			$this->emr = 'EMR #9';
            $this->client_id = '2058dcfc-7634-11e8-92d2-001851b9d605';
            $this->emr_number = 9;
		}

		public function get_emrs() {
    		$CI =& get_instance();
            
            $sql = "SELECT b.FIRST_NAME, b.LAST_NAME, a.EMR_ID, c.EMR_NAME
                    FROM CLIENTS_EMR a
                    INNER JOIN CLIENTS b
                    ON b.CLIENT_ID = a.CLIENT_ID
                    INNER JOIN EMRS c
                    ON c.EMR_ID=a.EMR_ID
                    WHERE a.CLIENT_ID = '".$this->client_id."'";

            $query = $CI->db->query($sql);
            if ($query->num_rows() > 0) {
                $data = $query->result();

                return $data;
                $CI->db->close();
            }
            return false;
        }

}