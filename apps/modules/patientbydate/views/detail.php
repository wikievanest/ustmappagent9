<section class="app-content">
    <div class="row">
      <!-- DOM dataTable -->
      <div class="col-md-12">
        <div class="widget">
          <header class="widget-header">
            <h4 class="widget-title">Total Encounters by <?php echo $title;?></h4>
            <div style="float:right;margin-top: -25px"><a href="<?=base_url('encounterstodate/all');?>" class="btn btn-primary"><i class="zmdi zmdi-long-arrow-left zmdi-hc-lg"></i> Back</a></div>
          </header><!-- .widget-header -->
          <hr class="widget-separator">
          <div class="widget-body">
            <div class="table-responsive">
              <table id="default-datatable" data-plugin="DataTable" class="table table-striped" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th style="width: 6%;text-align:center">ID</th>
                    <th style="width: 12%">Full Name</th>
                    <th style="width: 10%;text-align:center">DOB</th>
                    <th style="width: 10%;text-align:center">Facility</th>
                    <th style="width: 10%;text-align:center">Sensitivity</th>
                    <th style="width: 30%;text-align:center">Reason</th>
                    <th style="width: 5%;text-align:center">Count</th>
                    <th style="width: 8%;text-align:center">Last Input</th>
                  </tr>
                </thead>
                <tfoot>
                   <tr>
                    <th style="width: 6%;text-align:center">ID</th>
                    <th style="width: 12%">Full Name</th>
                    <th style="width: 10%;text-align:center">DOB</th>
                    <th style="width: 10%;text-align:center">Facility</th>
                    <th style="width: 10%;text-align:center">Sensitivity</th>
                    <th style="width: 30%;text-align:center">Reason</th>
                    <th style="width: 5%;text-align:center">Count</th>
                    <th style="width: 8%;text-align:center">Last Input</th>
                  </tr>
                </tfoot>
                <tbody>
                  <?php
                  $i=1;
                  foreach ($encounter as $ec) {
                ?>
                  <tr>
                    <td style="width: 6%;text-align:center"><?php echo $i;?></td>
                    <td style="width: 12%"><?php echo $ec->fname.', '.$ec->mname.' '.$ec->lname;?></td>
                    <td style="width: 10%;text-align:center"><?php echo $ec->DOB;?></td>
                    <td style="width: 10%;text-align:center"><?php echo $ec->facility;?></td>
                    <td style="width: 10%;text-align:center"><?php echo $ec->sensitivity;?></td>
                    <td style="width: 30%;text-align:center"><?php echo $ec->reason;?></td>
                    <td style="width: 5%;text-align:center"><?php echo $ec->jml;?></strong></td>
                    <td style="width: 8%;text-align:center"><strong><?php echo $ec->date;?></strong></td>
                  </tr>
                  <?php
                  $i++;
                    }
                  ?>
                </tbody>
                </tbody>
              </table>
            </div>
          </div><!-- .widget-body -->
        </div><!-- .widget -->
      </div><!-- END column -->
    </div><!-- .row -->
  </section><!-- .app-content -->