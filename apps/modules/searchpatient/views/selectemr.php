<section class="app-content">
	<div class="row">
		<div class="col-md-12">
			<div class="widget">
				<header class="widget-header">
					<h4 class="widget-title">Search Patient Information</h4>
				</header><!-- .widget-header -->
				<hr class="widget-separator">
				<div class="widget-body">
					<div class="m-b-xl">
							<?php 
								$submit = "searchpatient/all";
					            $attributes = array('role' => 'form'
					                , 'id' => 'form_add', 'name' => 'form_add','class' => 'form-inline', 'onSubmit' => 'document.getElementById(\'btn\').disabled=true;');
					            echo form_open($submit,$attributes); 
					        ?>
							<div class="form-group">
								<label for="exampleInputName2">First Name : </label>
								<input type="text" id="first_name" class="form-control" name="first_name" placeholder="First Name">
							</div>
							&nbsp;
							<div class="form-group">
								<label for="exampleInputEmail2">Last Name :</label>
								<input type="text" id="last_name" class="form-control" name="last_name" placeholder="Last Name">
							</div>
							&nbsp;
							<div class="form-group">
								<label for="exampleInputEmail2">Select EMR :</label>
								<select id="emr" class="form-control" name="emr">
								<?php
										foreach ($emrs as $emr) {
									?>
									<option value="<?php echo $emr->EMR_ID;?>"><?php echo $emr->EMR_NAME;?></option>
									<?php
										} 
									?>
								</select>
							</div>
							&nbsp;
							<button type="submit" class="btn btn-primary" id="btn"><i class="menu-icon zmdi zmdi-search zmdi-hc-lg"></i> Search</button>
						<?php echo form_close(); ?>
					</div>
				</div><!-- .widget-body -->
			</div><!-- .widget -->
		</div><!-- END column -->
	</div><!-- .row -->
</section><!-- #dash-content -->

	