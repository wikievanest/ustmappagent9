<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @FileInfo: /Users/wikie/Development/Sites/ustmappbruce/apps/modules/home/controllers/Home.php
 * @Author: wikie
 * @Email: wikieonline@gmail.com
 * @Date: 2016-07-19 11:04:00
 * @Last Modified by:   wikie
 * @Last Modified time: 2017-08-17 16:14:52
 */


class Home extends MY_Controller {
    var $title;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Home_qry','home');
        $this->auth->is_login();
        $this->auth->check_access();
        $this->title = 'Dashboard';
    }

    public function index() {   
        $data['emr'] = $this->home->get_emr_count();
        $level = $this->session->userdata('level');
        $data['title'] = $this->title;
        if($level == 1 || $level == 5) {
           $data['appointment_count'] = $this->home->get_count_all_appoinments();
            $data['patient_count'] = $this->home->get_count_all_patient_today();
            $data['encounter_count'] = $this->home->get_count_all_encounters();
            $data['patient'] = $this->home->get_all_patient_today();  
            $data['encounter'] = $this->home->get_all_encounter_today();    
            $data['patientcount'] = $this->home->getpatientcount();
            $data['encountercount'] = $this->home->getencountercount();
            $this->template
                ->title('Home','USTM Agent Dashboard')
                ->set_layout('main')
                ->build('index',$data); 
            } else if($level == 3) {
                $data['staff_encounter_count'] = $this->home->get_staff_encounter_count();
                $data['staff_encounters'] = $this->home->get_staff_encounters();
                $this->template
                    ->title('Home','USTM Agent Dashboard')
                    ->set_layout('main')
                    ->build('staffindex',$data);
            } else if($level == 4) {
                $data['md_patients_count'] = $this->home->get_md_patients_count();
                $data['md_patients'] = $this->home->get_md_patients();
                $this->template
                    ->title('Home','USTM Agent Dashboard')
                    ->set_layout('main')
                    ->build('mdindex',$data);
            }
        
    }

    
}
