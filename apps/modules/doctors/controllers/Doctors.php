<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @FileInfo: /Users/wikie/Development/Sites/ustmappbruce/apps/modules/doctors/controllers/Doctors.php
 * @Author: wikie
 * @Email: wikieonline@gmail.com
 * @Date: 2016-07-19 11:04:00
 * @Last Modified by:   wikie
 * @Last Modified time: 2017-07-27 03:43:24
 */

class Doctors extends MY_Controller {
    var $title;
    public function __construct() {
        parent::__construct();
        $this->load->model("Doctors_qry",'doctors');
        $this->auth->is_login();
        $this->auth->check_access();
        $this->title = 'Providers';
    }

    public function index() {
        $data['doctors'] = $this->doctors->get_doctors();
        $data['title'] = $this->title;
        $this->template
            ->title('USTM doctors','USTM Agent Dashboard')
            ->set_layout('main')
            ->build('index',$data);
    }
    
}
