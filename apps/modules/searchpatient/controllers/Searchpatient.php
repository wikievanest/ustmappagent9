<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @FileInfo: /Users/wikie/Development/Sites/ustmappbruce/apps/modules/searchpatient/controllers/Searchpatient.php
 * @Author: wikie
 * @Email: wikieonline@gmail.com
 * @Date: 2016-07-19 11:04:00
 * @Last Modified by:   wikie
 * @Last Modified time: 2017-07-27 02:54:43
 */

class Searchpatient extends MY_Controller {

    var $title;
    public function __construct() {
        parent::__construct();
        $this->load->model("Searchpatient_qry",'encounter');
        $this->auth->is_login();
        $this->auth->check_access();
        $this->title = 'Search Patients';
    }

    public function index() {
        $data['emrs'] = $this->emrs->get_emrs();
        $data['emr'] = $this->encounter->get_emr_count();
        $data['doctor'] = $this->encounter->get_all_doctor();
        $data['title'] = $this->title;
        $this->template
            ->title('Search Patient Information','USTM Agent Dashboard')
            ->set_layout('main')
            ->build('selectemr',$data);
    }

    public function all() {  
        $emr = $this->emrs->emr_number;
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        if($emr <> '') {
            $data['patient'] = $this->encounter->get_all_patient($emr,$first_name,$last_name);
            $this->template
                ->title('Search Patient Information','USTM Agent Dashboard')
                ->set_layout('main')
                ->build('index',$data);
        } else {
            $this->index();
        }
        
    }    
}
