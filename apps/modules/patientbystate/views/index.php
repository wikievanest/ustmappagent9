<script type="text/javascript">
  $(document).ready(function() {
    $('#responsive-datatable').DataTable({});
} );
</script>
<section class="app-content">
    <div class="row">
      <!-- DOM dataTable -->
      <div class="col-md-12">
        <div class="widget">
          <header class="widget-header">
            <h4 class="widget-title">Patient List by State <?php echo $titlex;?></h4>
            <div style="float:right;margin-top: -25px">
            <?php
            if (is_array($patient)) {
            ?>
            <a href="<?=base_url('patientbystate/export/'.$input_date.'/'.$to_date.'/'.$state.'_'.$emr);?>" target="_blank" class="btn btn-primary" id="btn" onclick="javascript:document.getElementById('btn').disabled=true;"><i class="zmdi zmdi-download zmdi-hc-lg"></i> Download</a>
            <?php } ?>
            &nbsp;<a href="<?=base_url('patientbystate');?>" class="btn btn-default"><i class="zmdi zmdi-long-arrow-left zmdi-hc-lg"></i> Back</a></div>
          </header><!-- .widget-header -->
          <hr class="widget-separator">
          <div class="widget-body">
            <table id="responsive-datatable" class="table table-striped" data-plugin="DataTable" cellspacing="0" width="100%">
              <thead>
                  <tr>
                    <th style="width: 6%;text-align:center">ID</th>
                    <th style="width: 18%">Full Name</th>
                    <th style="width: 10%;text-align:center">DOB</th>
                    <th style="width: 15%;text-align:left">City</th>
                    <th style="width: 6%;text-align:center">State</th>
                    <th style="width: 10%;text-align:center">Zip</th>
                    <th style="width: 15%;text-align:center">Input Date</th>
                    <th style="width: 5%;text-align:center">Script</th>
                    <th style="width: 5%;text-align:center">CCR</th>
                    <th style="width: 10%;text-align:center">EMR</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th style="width: 6%;text-align:center">ID</th>
                    <th style="width: 18%">Full Name</th>
                    <th style="width: 10%;text-align:center">DOB</th>
                    <th style="width: 15%;text-align:left">City</th>
                    <th style="width: 6%;text-align:center">State</th>
                    <th style="width: 10%;text-align:center">Zip</th>
                    <th style="width: 15%;text-align:center">Input Date</th>
                    <th style="width: 5%;text-align:center">Script</th>
                    <th style="width: 5%;text-align:center">CCR</th>
                    <th style="width: 10%;text-align:center">EMR</th>
                  </tr>
                </tfoot>
                <tbody>
                  <?php
                  foreach ($patient as $pt) {
                ?>
                  <tr>
                    <td style="width: 6%;text-align:center"><?php echo $pt->pid;?></td>
                    <td style="width: 18%"><?php echo $pt->fullname;?></td>
                    <td style="width: 10%;text-align:center"><?php echo $pt->dob;?></td>
                    <td style="width: 15%;text-align:left"><?php echo $pt->city;?></td>
                    <td style="width: 6%;text-align:center"><?php echo $pt->state;?></td>
                    <td style="width: 10%;text-align:center"><?php echo $pt->postal_code;?></td>
                    <td style="width: 15%;text-align:center"><?php echo $pt->date;?></td>
                    <td style="width: 5%;text-align:center"><strong><?php echo $pt->script_status;?></strong></td>
                    <td style="width: 5%;text-align:center"><strong><?php echo $pt->ccr_status;?></strong></td>
                    <td style="width: 10%;text-align:center"><strong><?php echo $pt->emr;?></strong></td>
                  </tr>
                  <?php
                    }
                  ?>
                </tbody>
                </tbody>
            </table>
          </div><!-- .widget-body -->
        </div><!-- .widget -->
      </div><!-- END column -->
    </div><!-- .row -->
  </section><!-- .app-content -->