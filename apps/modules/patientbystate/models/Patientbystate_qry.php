<?php
/**
 * @FileInfo: /Users/wikie/Development/Sites/ustmapp7/apps/modules/patientbystate/models/Patientbystate_qry.php
 * @Author: wikie
 * @Email: wikieonline@gmail.com
 * @Date: 2016-05-25 22:07:47
 * @Last Modified by:   wikie
 * @Last Modified time: 2017-08-19 05:01:07
 */

class Patientbystate_qry extends CI_Model{
    //put your code here
    protected $emr;
    public function __construct() {
        parent::__construct();
    }

    public function get_all_doctor() {
        $sql = "SELECT * FROM all_doctors
                group by fname,lname
                order by fname";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->result();

            return $data;
            $this->db->close();
        }
        return false;
    }

    public function get_states() {
      $sql = "SELECT zone_code, zone_name
              FROM emrapp15.geo_zone_reference";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->result();

            return $data;
            $this->db->close();
        }
        return false;
    }


    public function get_all_patient($emr,$state, $input_date,$to_date) {
        if($emr <> '') {
          $emr = "EMR #$emr";
        } else {
          $emr='';
        }
        if($input_date <> '' AND $to_date <> '') {
            $filter = "AND date BETWEEN '$input_date' AND '$to_date'";
        } 
        if($state <> '') {
            $states = "AND state = '$state'";
        } 
        $where = "WHERE emr LIKE '%$emr%' $filter $states";
        $sql = "SELECT pid,
                  CONCAT(fname,', ',
                  mname,' ' ,
                  lname) fullname,
                  dob,
                  city,
                  sex,
                  state,
                  postal_code,
                  date,
                  script_status,
                  ccr_status,
                  emr
                FROM all_patients_detail
                $where";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->result();

            return $data;
            $this->db->close();
        }
        return false;
    }
    public function get_detail_encounter($provider,$emr) {
        $db = 'emr'.$emr;
        $this->emr = $this->load->database($db,TRUE);
        $sql = "SELECT b.mdfname,c.fname,c.lname,c.DOB,a.reason,a.sensitivity,a.date,a.jml,a.facility 
            from (select  pid,provider_id,COUNT(id) jml,reason,sensitivity,DATE_FORMAT(date,'%Y-%m-%d') date,facility from form_encounter
            group by pid,provider_id
            HAVING provider_id <> 1) a
            INNER JOIN (select id,fname mdfname,mname mdmname,lname mdlname from users) b
            ON b.id=a.provider_id
            INNER JOIN (select pid,fname,lname,DOB from patient_data) c
            ON c.pid=a.pid
            where b.mdfname='$provider'
            order by a.date DESC";
        $query = $this->emr->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->result();

            return $data;
            $this->db->close();
        }
        return false;
    }
}
