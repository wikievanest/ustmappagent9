<?php
/**
 * @FileInfo: /Users/wikie/Development/Sites/ustmapp7/apps/modules/home/models/Home_qry.php
 * @Author: wikie
 * @Email: wikieonline@gmail.com
 * @Date: 2016-05-25 22:07:47
 * @Last Modified by:   wikie
 * @Last Modified time: 2017-08-19 05:00:15
 */

class Home_qry extends CI_Model{
    //put your code here
    public function __construct() {
        parent::__construct();
    }

    public function get_count_all_appoinments() {
        $sql = "SELECT SUM(jml) jml FROM all_appointments_today
                WHERE emr='".$this->emrs->emr."'";
        $query = $this->db->query($sql);
        $result = $query->row();
        return $result;
        $this->db->close();
    }

    public function get_count_all_encounters() {
        $sql = "SELECT SUM(jml) jml FROM all_encounters_count_today
                WHERE emr='".$this->emrs->emr."'";
        $query = $this->db->query($sql);
        $result = $query->row();
        return $result;
        $this->db->close();
    }

    public function get_count_all_patient_today() {
        $sql = "SELECT SUM(jml) jml FROM all_patients_count_today
                WHERE emr='".$this->emrs->emr."'";
        $query = $this->db->query($sql);
        $result = $query->row();
        return $result;
        $this->db->close();
    }

    public function get_all_patient_today() {
    	$sql = "SELECT pid,fname,lname,mname,dob,city,state,postal_code,date,script_status,ccr_status,emr 
                FROM  all_patients_today
                WHERE emr='".$this->emrs->emr."'
                order by emr,date";
    	$query = $this->db->query($sql);
    	$result = $query->result();
    	return $result;
        $this->db->close();
    }

    public function get_all_encounter_today() {
        $sql = "SELECT encounter,
                    fname,
                    lname,
                    mname,
                    DOB,
                    reason,
                    facility,
                    sensitivity,
                    mdfname,
                    mdmname,
                    mdlname,
                    date,
                    script_status,
                    ccr_status,
                    emr
                FROM all_encounters_today
                WHERE emr='".$this->emrs->emr."'";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
        $this->db->close();
    }

    public function getpatientcount() {
        $sql = "SELECT
                  SUM(IF (MONTH = 1, jml, 0)) AS 'January',
                SUM(IF (MONTH = 2, jml, 0)) AS 'February',
                SUM(IF (MONTH = 3, jml, 0)) AS 'March',
                SUM(IF (MONTH = 4, jml, 0)) AS 'April',
                SUM(IF (MONTH = 5, jml, 0)) AS 'May',
                SUM(IF (MONTH = 6, jml, 0)) AS 'June',
                SUM(IF (MONTH = 7, jml, 0)) AS 'July',
                SUM(IF (MONTH = 8, jml, 0)) AS 'August',
                SUM(IF (MONTH = 9, jml, 0)) AS 'September',
                SUM(IF (MONTH = 10, jml, 0)) AS 'October',
                SUM(IF (MONTH = 11, jml, 0)) AS 'November',
                SUM(IF (MONTH = 12, jml, 0)) AS 'December',
                'EMR #15' emr 
                FROM (SELECT
                  COUNT(pid) jml,
                  YEAR(regdate) AS 'year',
                  MONTH(regdate) AS 'month'
                FROM emrapp15.patient_data
                GROUP BY year,
                         month
                HAVING year = YEAR(NOW())
                ORDER BY regdate ASC) AS patient
                ";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
        $this->db->close();
    }

    public function getencountercount() {
        $sql = "SELECT
                  SUM(IF (MONTH = 1, jml, 0)) AS 'January',
                SUM(IF (MONTH = 2, jml, 0)) AS 'February',
                SUM(IF (MONTH = 3, jml, 0)) AS 'March',
                SUM(IF (MONTH = 4, jml, 0)) AS 'April',
                SUM(IF (MONTH = 5, jml, 0)) AS 'May',
                SUM(IF (MONTH = 6, jml, 0)) AS 'June',
                SUM(IF (MONTH = 7, jml, 0)) AS 'July',
                SUM(IF (MONTH = 8, jml, 0)) AS 'August',
                SUM(IF (MONTH = 9, jml, 0)) AS 'September',
                SUM(IF (MONTH = 10, jml, 0)) AS 'October',
                SUM(IF (MONTH = 11, jml, 0)) AS 'November',
                SUM(IF (MONTH = 12, jml, 0)) AS 'December',
                'EMR #15' emr
                FROM (SELECT
                  COUNT(id) jml,
                  year,
                  month
                FROM (SELECT
                  id,
                  YEAR(date) AS 'year',
                  MONTH(date) AS 'month'
                FROM emrapp15.form_encounter
                WHERE YEAR(date) = YEAR(NOW())
                AND provider_id <> 1) encounter
                GROUP BY year,
                         month
                ORDER BY month ASC) b";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
        $this->db->close();
    }

    public function get_emr_count() {
        $sql = "SELECT * from emr_active";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->row();

            return $data->jml;
            $this->db->close();
        }
        return false;
    }

    

}
