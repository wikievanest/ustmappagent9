<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @FileInfo: /Users/wikie/Development/Sites/ustmappbruce/apps/modules/patientbyencounter/controllers/Patientbyencounter.php
 * @Author: wikie
 * @Email: wikieonline@gmail.com
 * @Date: 2016-07-19 11:04:00
 * @Last Modified by:   wikie
 * @Last Modified time: 2017-07-27 03:32:27
 */

class Patientbyencounter extends MY_Controller {
    var $title;
    public function __construct() {
        parent::__construct();
        $this->load->model("Patientbyencounter_qry",'patient');
        $this->auth->is_login();
        $this->auth->check_access();
        $this->title = 'Patients';
    }

    public function index() {
        $data['emrs'] = $this->emrs->get_emrs();
        $data['states'] = $this->patient->get_states();
        $data['title'] = 'Patients';
        $this->template
            ->title('Total Patient by Encounter','USTM Agent Dashboard')
            ->set_layout('main')
            ->build('selectemr',$data);
    }

    public function all() {  
        $data['emr'] = $this->input->post('emr');
        $data['input_date'] = $this->input->post('input_date');
        $data['to_date'] = $this->input->post('to_date');
        $data['state'] = $this->input->post('state');
        if($data['emr'] <> '') {
            $data['patient'] = $this->patient->get_all_patient($this->emrs->emr_number,$data['state'],$data['input_date'],$data['to_date']);
            $data['titlex'] = "from ".$data['input_date']." to ".$data['to_date'];
            $data['title'] = 'Patients';
            $this->template
                ->title('Patients by Encounter','USTM Agent Dashboard')
                ->set_layout('main')
                ->build('index',$data);
        } else {
            $this->index();
        }
        
    }

    public function export() {
        $this->load->library('PHPExcel');
        $emrs= $this->uri->segment(5);
        $data = explode('_', $emrs);
        $emr = $data['0'];
        $state = $data['1'];
        $input_date = $this->uri->segment(3);
        $to_date = $this->uri->segment(4);
        $patient = $this->patient->get_all_patient($this->emrs->emr_number,$state,$input_date,$to_date);
        if(is_array($patient)) {

            $objPHPExcel = new PHPExcel(); 
            $headings = array('NO','PID','FULL NAME', 'DOB', 'CITY','STATE','POSTAL CODE','DATE','SCRIPT STATUS','CCR STATUS','EMR'); 
            $objPHPExcel->getActiveSheet()->setTitle('Patient list'); 
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(16);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(16);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(16);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(18);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(18);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(18);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(18);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(18);
            $objPHPExcel->getActiveSheet()->getStyle('A')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('B')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('D')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('E')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('G')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('H')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('I')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('J')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('K')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('L')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('H1')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('I1')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('J1')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('K1')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('L1')->getFont()->setBold(true);
            $rowNumber = 1; 
            $col = 'A'; 
            foreach($headings as $heading) { 
               $objPHPExcel->getActiveSheet()->setCellValue($col.$rowNumber,$heading); 
               $col++; 
            } 

            // Loop through the result set 
            $rowNumber = 2; 
            foreach($patient as $pt) { 
               $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $rowNumber, $rowNumber-1);
               $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $rowNumber, $pt->pid);
               $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $rowNumber, $pt->fullname);
               $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $rowNumber, $pt->DOB);
               $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $rowNumber, $pt->city);
               $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $rowNumber, $pt->state);
               $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $rowNumber, $pt->postal_code);
               $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $rowNumber, $pt->date_encounter);
               $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $rowNumber, $pt->script_status);
               $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $rowNumber, $pt->ccr_status);
               $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $rowNumber, $pt->emr);
               $rowNumber++;
            } 

            // Freeze pane so that the heading line won't scroll 
            $objPHPExcel->getActiveSheet()->freezePane('A2'); 
            $filename = "Patient list by Encounter $input_date to $to_date.xls";
            // Save as an Excel BIFF (xls) file 
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
            header('Content-Type: application/vnd.ms-excel'); 
            header('Content-Disposition: attachment;filename="'.$filename.'"'); 
            header('Cache-Control: max-age=0'); 
            $objWriter->save('php://output'); 
            exit(); 
        } 

    }

    
}
