<script type="text/javascript">
	$(function () {
        $('#input_date').datetimepicker({
            format: 'YYYY-MM-DD'
        });
        $('#to_date').datetimepicker({
            format: 'YYYY-MM-DD'
        });
        $('#emr').chosen();
    });
	function redirect() {
		var emr = $("#emr").val();
		window.location.assign('<?=base_url('patientbydate');?>');
	}

</script>
<section class="app-content">
	<div class="row">
		<div class="col-md-12">
			<div class="widget">
				<header class="widget-header">
					<h4 class="widget-title">Total Patient List by date</h4>
				</header><!-- .widget-header -->
				<hr class="widget-separator">
				<div class="widget-body">
					<div class="m-b-xl">
							<?php 
								$submit = "patientbydate/all";
					            $attributes = array('role' => 'form'
					                , 'id' => 'form_add', 'name' => 'form_add','class' => 'form-inline', 'onSubmit' => 'document.getElementById(\'btn\').disabled=true;');
					            echo form_open($submit,$attributes); 
					        ?>
					        <div class="form-group">
								<label for="exampleInputEmail2">Select EMR :</label>
								<select id="emr" class="form-control" name="emr">
									<?php
										foreach ($emrs as $emr) {
									?>
									<option value="<?php echo $emr->EMR_ID;?>"><?php echo $emr->EMR_NAME;?></option>
									<?php
										} 
									?>
								</select>
							</div>
							&nbsp;
							<div class="form-group">
								<input style="min-width: 180px;" type="text" id="input_date" class="form-control" data-plugin="datetimepicker" name="input_date" placeholder="Date input from" required>
							</div>
							&nbsp;
							<div class="form-group">
								<input style="min-width: 180px;" type="text" id="to_date" class="form-control" name="to_date" placeholder="To Date" required>
							</div>
							&nbsp;
							<button type="submit" class="btn btn-primary" id="btn" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><i class="menu-icon zmdi zmdi-search zmdi-hc-lg"></i> Search</button>
						<?php echo form_close(); ?>
					</div>
				</div><!-- .widget-body -->
			</div><!-- .widget -->
		</div><!-- END column -->
	</div><!-- .row -->
</section><!-- #dash-content -->
