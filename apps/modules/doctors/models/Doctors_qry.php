<?php
/**
 * @FileInfo: /Users/wikie/Development/Sites/ustmapp7/apps/modules/doctors/models/Doctors_qry.php
 * @Author: wikie
 * @Email: wikieonline@gmail.com
 * @Date: 2016-05-25 22:07:47
 * @Last Modified by:   wikie
 * @Last Modified time: 2017-08-19 05:00:02
 */

class Doctors_qry extends CI_Model{
    //put your code here
    protected $emr;
    public function __construct() {
        parent::__construct();
    }

    public function get_doctors() {
        $sql = "SELECT user_id,
                  CONCAT(first_name,' ',
                  last_name) fullname,
                  email,
                  user_stat
                FROM USERS 
                where level=4 and user_stat=1";
        $sql = "SELECT
                  CONCAT(a.fname,' ',
                  a.lname) fullname
                FROM (emrapp15.users a
                JOIN emrapp15.form_encounter b
                  ON ((b.provider_id = a.id)))
                GROUP BY a.fname,
                         a.lname
                HAVING (a.fname <> 'Administrator')";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->result();


            return $data;
            $this->db->close();
        }
        return false;
    }
}
