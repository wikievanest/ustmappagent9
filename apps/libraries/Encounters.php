<?php
/**
 * @FileInfo: /Users/wikie/Development/Sites/ustmapp7/apps/libraries/Encounters.php
 * @Author: wikie
 * @Email: wikieonline@gmail.com
 * @Date: 2016-07-20 12:09:49
 * @Last Modified by:   wikie
 * @Last Modified time: 2017-08-19 04:59:06
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Encounters {

		public static $now;
		public static $yesterday;

		public static $n;
		public static $y;
		
		function __construct() {
			$this->now = date('Y-m-d');
			$this->yesterday = date('Y-m-d', strtotime($this->now . ' -1 day'));

			$this->n = date('Y/m/d');
			$this->y = date('Y/m/d', strtotime($this->now . ' -1 day'));
		}

}