<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Admin, Dashboard, Bootstrap" />
  <link rel="shortcut icon" sizes="196x196" href="">
  <title><?php echo $template['title'];?></title>
  
  <link rel="stylesheet" href="<?=base_url('public/assets/libs/bower/font-awesome/css/font-awesome.min.css');?>">
  <link rel="stylesheet" href="<?=base_url('public/assets/libs/bower/material-design-iconic-font/dist/css/material-design-iconic-font.css');?>">
  <!-- build:css -->
  <link rel="stylesheet" href="<?=base_url('public/assets/libs/bower/animate.css/animate.min.css');?>">
  <link rel="stylesheet" href="<?=base_url('public/assets/libs/bower/fullcalendar/dist/fullcalendar.min.css');?>">
  <link rel="stylesheet" href="<?=base_url('public/assets/libs/bower/perfect-scrollbar/css/perfect-scrollbar.css');?>">
  <link rel="stylesheet" href="<?=base_url('public/assets/css/bootstrap.css');?>">
  <link rel="stylesheet" href="<?=base_url('public/assets/css/core.css');?>">
  <link rel="stylesheet" href="<?=base_url('public/assets/css/app.css');?>">
  <link rel="stylesheet" href="<?=base_url('public/assets/libs/misc/datatables/datatables.min.css');?>">
  <link rel="stylesheet" href="<?=base_url('public/assets/libs/misc/chosen/chosen.css');?>">
  <link rel="stylesheet" href="<?=base_url('public/assets/libs/bower/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css');?>">
  <!-- endbuild -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700,800,900,300">
  <script src="<?=base_url('public/assets/libs/bower/jquery/dist/jquery.js');?>"></script>
  <script src="<?=base_url('public/assets/libs/bower/breakpoints.js/dist/breakpoints.min.js');?>"></script>
  <script>
    Breakpoints();
  </script>
</head>
  
<body class="menubar-left menubar-unfold menubar-light theme-primary" style="font-family: sans-serif;">
<!--============= start main area -->

<?php $this->load->view('partial/topnav');?>
<!--========== END app navbar -->

<?php $this->load->view('partial/sidebar');?>
<!--========== END app aside -->
<!-- APP MAIN ==========-->
<main id="app-main" class="app-main">
  <div class="wrap">
    <?php echo $template['body']; ?>
  </div><!-- .wrap -->
  <!-- APP FOOTER -->
  <?php $this->load->view('partial/footer');?>
  <!-- /#app-footer -->
</main>
<!--========== END app main -->

  <!-- build:js score.min.js-->

  <script src="<?=base_url('public/assets/libs/bower/jquery-ui/jquery-ui.min.js');?>"></script>
  <script src="<?=base_url('public/assets/libs/bower/jQuery-Storage-API/jquery.storageapi.min.js');?>"></script>
  <script src="<?=base_url('public/assets/libs/bower/bootstrap-sass/assets/javascripts/bootstrap.js');?>"></script>
  <script src="<?=base_url('public/assets/libs/bower/superfish/dist/js/hoverIntent.js');?>"></script>
  <script src="<?=base_url('public/assets/libs/bower/superfish/dist/js/superfish.js');?>"></script>
  <script src="<?=base_url('public/assets/libs/bower/switchery/dist/switchery.min.js');?>"></script>
  <script src="<?=base_url('public/assets/libs/bower/jquery-slimscroll/jquery.slimscroll.js');?>"></script>
   
  <script src="<?=base_url('public/assets/libs/bower/perfect-scrollbar/js/perfect-scrollbar.jquery.js');?>"></script>
  <script src="<?=base_url('public/assets/libs/bower/PACE/pace.min.js');?>"></script>
  <!-- endbuild -->

  <!-- build:js <?=base_url('public/assets/js/app.min.js');?> -->
  <script src="<?=base_url('public/assets/js/plugins.js');?>"></script>
  <script src="<?=base_url('public/assets/js/app.js');?>"></script>
  <!-- endbuild -->
  <!-- chart -->
  <script src="<?=base_url('public/assets/libs/misc/flot/jquery.flot.min.js');?>"></script>
  <script src="<?=base_url('public/assets/libs/misc/flot/jquery.flot.pie.min.js');?>"></script>
  <script src="<?=base_url('public/assets/libs/misc/flot/jquery.flot.stack.min.js');?>"></script>
  <script src="<?=base_url('public/assets/libs/misc/flot/jquery.flot.resize.min.js');?>"></script>
  <script src="<?=base_url('public/assets/libs/misc/flot/jquery.flot.curvedLines.js');?>"></script>
  <script src="<?=base_url('public/assets/libs/misc/flot/jquery.flot.tooltip.min.js');?>"></script>
  <script src="<?=base_url('public/assets/libs/misc/flot/jquery.flot.categories.min.js');?>"></script>
  <script src="<?=base_url('public/assets/libs/misc/echarts/build/dist/echarts-all.js');?>"></script>
  <script src="<?=base_url('public/assets/libs/misc/echarts/build/dist/theme.js');?>"></script>
  <script src="<?=base_url('public/assets/libs/misc/echarts/build/dist/jquery.echarts.js');?>"></script>
  <!-- end chart -->
  <!-- datatable -->
  <script src="<?=base_url('public/assets/libs/misc/datatables/datatables.min.js');?>"></script>
  <!-- Chosen -->
  <script src="<?=base_url('public/assets/libs/misc/chosen/chosen.jquery.min.js');?>"></script>
  <!-- end datatable -->
  <script src="<?=base_url('public/assets/libs/bower/moment/moment.js');?>"></script>
  <script src="<?=base_url('public/assets/libs/bower/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js');?>"></script>
  <script src="<?=base_url('public/assets/libs/bower/fullcalendar/dist/fullcalendar.min.js');?>"></script>
  <script src="<?=base_url('public/assets/js/fullcalendar.js');?>"></script>
</body>
</html>